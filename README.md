# README #

Hive Workshop

docker-hive-on-tez

https://drive.google.com/file/d/0ByJLBTmJojjzZTJQM2UzN2J6aUU/view

Pokemonid_Number: This column represents id of each Pokémon.
Name: This column represents the name of the Pokémon.
Type 1: This column represents the property of a Pokémon.
Type 2: This column represents the extended property of the same Pokémon.
A Pokémon may be one or both the types. For instance, Charmander is a Fire type, whileBulbasaur is both a Grass type as well as a Poison type. With the current 18-type system, there are 324 possible ways to assign these types to Pokémon, along with 171 unique combinations. As of Generation VI, 133 different type combinations have been used.
Total: This column represents the sum of all character points of a Pokémon (HP, Attack, Defense, Sp. Atk, Sp. Def, and Speed).
HP (Hit Points): This column represents Pokémon Hit Points, which is a value that determines how much damage a Pokémon can receive. When a Pokémon’s HP is down to ‘0’, the Pokémon will faint. HP is the most frequently affected stat of them all, as a depleting HP is a key factor in winning a battle.
Attack: This column represents the Attack stat.
Defense: This column represents the Defense stat.
Sp. Atk: This column represents a Pokémon’s Special Attack stat.
Sp. Def: This column represents a Pokémon’s Special Defense stat.
Speed: This column represents the speed stat of a Pokémon.


CREATE TABLE pokemon (Number Int,Name String,Type1 String,Type2 String,Total Int,HP Int,Attack Int,Defense Int,Sp_Atk Int,Sp_Def Int,Speed Int) row format delimited fields terminated BY ',' lines terminated BY '\n' tblproperties("skip.header.line.count"="1");

load data local inpath ‘/tmp/Pokemon.csv' INTO table pokemon;

Find out the average HP (Hit points) of all the Pokémon, using the below query.

Select avg(HP) from pokemon;


Create and insert values of existing table ‘pokemon’ into a new table ‘pokemon1’, with an additional column ‘power_rate’ to find the count of ‘powerful’ and ‘moderate’ from the table ‘pokemon1’.

create table pokemon1 as select *, IF(HP>69.25875, 'powerful', IF(HP<69.25875, 'Moderate','powerless')) AS power_rate from pokemon;

select COUNT(name),power_rate from pokemon1 group by power_rate;


Find out the top 10 Pokémons according to their HP’s using the below query.

select name,hp from pokemon1 order by hp desc limit 10;


Find out the top 10 Pokémons based on their Attack stat, using the below query.

select name,attack from pokemon1 order by attack desc limit 10;


Find out the top 10 Pokémons based on their Defense stat, using the below query.

select name,defense from pokemon1 order by defense desc limit 10;


Find out the top 10 Pokémons based on their total power.

select name,total from pokemon1 order by total desc limit 10;

Find out the top 10 Pokémons having a drastic change in their attack and sp.attack, using the below query.

select name,(attack-sp_atk) as atk_diff from pokemon1 order by atk_diff limit 10;

Find out the top 10 Pokémons having a drastic change in their defense and sp.defense, using the below query.

select name,(defense-sp_defense) as def_diff from pokemon1 order by def_diff limit 10;

Find out the top 10 fastest Pokémons, using the below query.

Select name, speed from pokemon order by speed desc limit 10;


— Book Dump example

http://www2.informatik.uni-freiburg.de/~cziegler/BX/


